/*
 * mm-naive.c - The fastest, least memory-efficient malloc package.
 * 
 * In this naive approach, a block is allocated by simply incrementing
 * the brk pointer.  A block is pure payload. There are no headers or
 * footers.  Blocks are never coalesced or reused. Realloc is
 * implemented directly using mm_malloc and mm_free.
 *
 * NOTE TO STUDENTS: Replace this header comment with your own header
 * comment that gives a high level description of your solution.
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in the following struct.
 ********************************************************/
team_t team = {
    /* Team name */
    "steinsgate",
    /* First member's full name */
    "Stein Kim",
    /* First member's email address */
    "wotjd1102@gmail.com",
    /* Second member's full name (leave blank if none) */
    "Sun Jeon",
    /* Second member's email address (leave blank if none) */
    "?"
};

// global scalar variables defined by us
static char *seg_lists;
static char *seg_list_begins;
static char *first_block;

// macro constants defined by us

/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* Basic constants and macros */
#define WSIZE 4 /* Word and header/footer size (bytes) */
#define DSIZE 8 /* Double word size (bytes) */
#define CHUNKSIZE (1<<12) /* Extend heap by this amount (bytes) */
#define NUM_LIST 9

// macro functions defined by us
#define MAX(x, y) ((x) > (y)? (x) : (y))

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)
#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc) ((size) | (alloc))

/* Read and write a word at address p */
#define GET(p) (*(unsigned int *)(p))
#define PUT(p, val) (*(unsigned int *)(p) = (val))

/* Read the size and allocated fields from address p */
#define GET_SIZE(p) (GET(p) & ~0x7)
#define GET_ALLOC(p) (GET(p) & 0x1)

/* Given block ptr bp, compute address of its header and footer */
/* set header first, then footer */
#define HDRP(bp) ((char *)(bp) - WSIZE)
#define FTRP(bp) ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE)

/* Given block ptr bp, compute address of next and previous blocks */
#define NEXT_BLKP(bp) ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)))
#define PREV_BLKP(bp) ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)))

// static function defined by us

static size_t align(size_t size) {
    return size % DSIZE ? size + DSIZE - size % DSIZE : size;
} 
static void *get_list_next(void *block) {
    return GET((char *)block + WSIZE);
}

static void *get_list_prev(void *block) {
    return GET((char *)block);
}

static void set_list_next(void *block, void *next) {
    PUT((char *)block + WSIZE, next);
}

static void set_list_prev(void *block, void *prev) {
    PUT((char *)block, prev);
}

// begin and end always exist. but front and back doesn't
// now list block size is two words.
static void *list_begin(void *list) {
    return GET(list);
}

static void *list_end(void *list) {
    return GET(list + WSIZE);
}

static void *list_front(void *list) {
    return get_list_next(list_begin(list));
}

static void *list_back(void *list) {
    return get_list_prev(list_end(list));
}

static int list_empty(void *list) {
    return get_list_next(list_begin(list)) == list_end(list) &&
           get_list_prev(list_end(list)) == list_begin(list);
}

// push block to list
static void list_push_front(void *list, void *block) {
    void *old_front = list_front(list);
    void *begin = list_begin(list);
    set_list_prev(block, begin);
    set_list_next(block, old_front);
    set_list_next(begin, block);
    set_list_prev(old_front, block);
}

static void list_push_back(void *list, void *block) {
    void *old_back = list_back(list);
    void *end = list_end(list);
    set_list_prev(block, old_back);
    set_list_next(block, end);
    set_list_next(old_back, block);
    set_list_prev(end, block);
}

static void list_remove(void *block) {
    void *old_prev = get_list_prev(block);
    void *old_next = get_list_next(block);
    assert(old_prev && old_next);
    set_list_next(old_prev, old_next);
    set_list_prev(old_next, old_prev);
    set_list_next(block, NULL);
    set_list_prev(block, NULL);
}

static void *get_class_list(size_t size) {
    assert(size > 0);
    if (size >= 4096) {
        return seg_lists + (NUM_LIST-1)*DSIZE;
    }
    // first 4 seg lists were ommitted
    int i = -4;
    size_t size_temp = size;
    while (size_temp >>= 1) {
        i++;
    }
    return seg_lists+i*DSIZE;
}

static void *split(void *bp, size_t size) {

    assert(size % DSIZE == 0);
    void *remain; 
    size_t remain_size = GET_SIZE(HDRP(bp)) - size;
    assert(remain_size >= 0);

    // remove block from free list
    list_remove(bp);

    // minimum size for remain is 4 times word size(header, footer, prev blk, next blk).
    if (remain_size < 4*WSIZE) {
        // set new header and footer of bp
        PUT(HDRP(bp), PACK(size + remain_size, 1));
        PUT(FTRP(bp), PACK(size + remain_size, 1));
        return bp;
    }

    // set new header and footer of bp
    PUT(HDRP(bp), PACK(size, 1));
    PUT(FTRP(bp), PACK(size, 1));

    
    remain = NEXT_BLKP(bp);
    PUT(HDRP(remain), PACK(remain_size, 0));
    PUT(FTRP(remain), PACK(remain_size, 0));

    // push remainder to list
    list_push_back(get_class_list(remain_size), remain);
    
    return bp;
}

static void *coalesce(void *bp) {
    assert(PREV_BLKP(bp) && NEXT_BLKP(bp));
    size_t prev_alloc = GET_ALLOC(FTRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    size_t size = GET_SIZE(HDRP(bp));

    if (prev_alloc && next_alloc) { /* Case 1 */
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size, 0));
    }

    else if (prev_alloc && !next_alloc) { /* Case 2 */

        // remove from free list
        list_remove(NEXT_BLKP(bp));

        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size, 0));
    }

    else if (!prev_alloc && next_alloc) { /* Case 3 */

        // remove from free list
        list_remove(PREV_BLKP(bp));

        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }

    else { /* Case 4 */

        // remove from free list
        list_remove(PREV_BLKP(bp));
        list_remove(NEXT_BLKP(bp));

        size += GET_SIZE(HDRP(PREV_BLKP(bp))) + GET_SIZE(FTRP(NEXT_BLKP(bp)));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }

    // add to free list
    list_push_back(get_class_list(size), bp);

    return bp;
}

static void *extend_heap(size_t words) 
{
    char *bp;
    size_t size;

    /* Allocate an even number of words to maintain alignment */
    size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
    if ((long) (bp = mem_sbrk(size)) == -1) {
        return NULL;
    }
    
    /* Initialize free block header/footer and the epilogue header. */
    PUT(HDRP(bp), PACK(size, 0));           /* Free block header */
    PUT(FTRP(bp), PACK(size, 0));           /* Free block footer */
    PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1));   /* New epilogue header */

    /* Coalesce if the previous block was free */
    return coalesce(bp);
}

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    int i;
    // get words for free lists.
    // 13 - 4 = 9 seg lists. first 4 seg lists ommitted.
    // one double word is for padding(null termination)
    if ((seg_lists = mem_sbrk((NUM_LIST+1)*DSIZE)) == (void *)-1) {
        return -1;
    }
    // make begin, end for each seg list.
    if ((seg_list_begins = mem_sbrk(2*NUM_LIST*DSIZE)) == (void *)-1) {
        return -1;
    }

    // null termination after seg_lists part.
    PUT(seg_lists+NUM_LIST*DSIZE, 0);
    PUT(seg_lists+NUM_LIST*DSIZE+WSIZE, 0);

    // initializing lists.
    for (i=0; i < NUM_LIST; i++) {
        void *list = seg_lists+i*DSIZE;
        void *begin = seg_list_begins+(2*i)*DSIZE;
        void *end = seg_list_begins+(2*i+1)*DSIZE;
        PUT(list, begin);
        PUT(list + WSIZE, end);
        // list begin
        set_list_next(begin, end);
        set_list_prev(begin, NULL);
        // list end
        set_list_next(end, NULL);
        set_list_prev(end, begin);
    }
    if ((first_block = mem_sbrk(4*WSIZE)) == (void *)-1) {
        return -1;
    }
    PUT(first_block, 0);                            /* align padding */
    PUT(first_block + (1*WSIZE), PACK(DSIZE, 1));   /* Prologue header */
    PUT(first_block + (2*WSIZE), PACK(DSIZE, 1));   /* Prologue footer */
    PUT(first_block + (3*WSIZE), PACK(0, 1));       /* Epilogue header */
    first_block += (2*WSIZE);

    first_block = (char *)extend_heap(CHUNKSIZE/WSIZE);
    if (first_block == NULL) {
        return -1;
    }

    return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
    int newsize = align(size + DSIZE);
    
    // find suitable block from class list
    void *itr_block;
    void *class_list;
    for (class_list = get_class_list(newsize); class_list < seg_lists + NUM_LIST*DSIZE; class_list += DSIZE) {
        for (itr_block = list_front(class_list); itr_block != list_end(class_list); itr_block = get_list_next(itr_block)) {
            if (GET_SIZE(HDRP(itr_block)) >= newsize) {
                return split(itr_block, newsize);
            }
        }
    }

    // get more memory from os
    void *bp = extend_heap(MAX(CHUNKSIZE, newsize)/WSIZE);
    if (bp == NULL) {
        return NULL;
    }

    return split(bp, newsize);
}

/*
 * mm_free - Freeing a block does nothing.
 */
void mm_free(void *ptr)
{
    // set new header
    size_t size = GET_SIZE(HDRP(ptr));
    PUT(HDRP(ptr), PACK(size, 0));           /* Free block header */
    PUT(FTRP(ptr), PACK(size, 0));           /* Free block footer */
    return coalesce(ptr);
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *ptr, size_t size)
{
    if (ptr == NULL) {
        return mm_malloc(size);
    }
    if (size == 0) {
        mm_free(ptr);
        return NULL;
    }
    size_t newSize = align(size + DSIZE);
    size_t remainSize = GET_SIZE(HDRP(ptr)) - newSize;
    if (GET_SIZE(HDRP(ptr)) < newSize) {
        void *oldptr = ptr;
        void *newptr;
        size_t copySize;
        
        newptr = mm_malloc(size);
        if (newptr == NULL) {
            return NULL;
        }
        copySize = GET_SIZE(HDRP(oldptr));
        if (size < copySize)
        copySize = size;
        memcpy(newptr, oldptr, copySize);
        mm_free(oldptr);
        return newptr;
    }
    if (remainSize >= 4*WSIZE) {
        // set new header and footer of ptr        
        PUT(HDRP(ptr), PACK(newSize, 1));
        PUT(FTRP(ptr), PACK(newSize, 1));
        void *remain = NEXT_BLKP(ptr);
        PUT(HDRP(remain), PACK(remainSize, 0));
        PUT(FTRP(remain), PACK(remainSize, 0));
        list_push_back(get_class_list(remainSize), remain);
    }
    return ptr;
}














